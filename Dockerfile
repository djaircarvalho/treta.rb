FROM ruby:2.6-slim-buster
LABEL maintainer="Djair Carvalho <djaircarvalho.dj7@gmail.com>"

WORKDIR /app

RUN apt-get update -qq && apt-get install -qq -y \
  build-essential --no-install-recommends && gem install bundler:2.2.25

ADD Gemfile* ./
RUN bundle install
COPY . .

CMD ["ruby", "treta.rb"]

