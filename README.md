# A simple Discord Bot to get tretas from [TretaDev](https://github.com/pilhacheia/tretadev/issues)

## Building 

```
$ docker build . -t treta:latest
```

## Running

```
$ docker run -e TOKEN=niceToken treta
```
