require 'discordrb'

bot = Discordrb::Commands::CommandBot.new token: ENV['TOKEN'], prefix: '!'

bot.message(with_text: 'Ping!') do |event|
  event.respond 'Pong!'
end

bot.command :treta do |event|
  event.respond "https://github.com/pilhacheia/tretadev/issues/#{rand 74}"
end

bot.command :user do |event|
  event.user.name
end

bot.run
